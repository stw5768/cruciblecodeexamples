import java.util.Random;

public class RandomNumber{
  public static void main(String[] args){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    System.out.println("Generated number: " + randomInt);
  }
}
